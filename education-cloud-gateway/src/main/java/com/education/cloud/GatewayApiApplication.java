/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.education.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.education.cloud.gateway.filter.FilterPre;

/**
 * 服务网关
 *
 * @author wujing
 */
@EnableZuulProxy
@SpringCloudApplication
public class GatewayApiApplication {


	public static void main(String[] args) {
		SpringApplication.run(GatewayApiApplication.class, args);
	}


}
