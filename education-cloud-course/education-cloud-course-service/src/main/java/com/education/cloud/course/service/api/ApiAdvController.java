package com.education.cloud.course.service.api;

import com.education.cloud.course.common.bo.AdvBO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.education.cloud.course.service.api.biz.ApiAdvBiz;
import com.education.cloud.course.common.dto.AdvListDTO;
import com.education.cloud.util.base.BaseController;
import com.education.cloud.util.base.Result;

import io.swagger.annotations.ApiOperation;

/**
 * 广告信息
 *
 * @author wujing
 */
@Api(tags = "广告列表接口")
@RestController
@RequestMapping(value = "/course/api/adv")
public class ApiAdvController extends BaseController {

	@Autowired
	private ApiAdvBiz biz;

	@ApiOperation(value = "广告列表接口", notes = "首页轮播广告列出")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Result<AdvListDTO> list(@RequestBody AdvBO advBO) {
		return biz.list(advBO);
	}

}
